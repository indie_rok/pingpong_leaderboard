class CreateMatches < ActiveRecord::Migration
  def change
    create_table :matches do |t|
      t.integer :user_a_id
      t.integer :user_b_id
      t.integer :score_a
      t.integer :score_b
      t.date :date

      t.timestamps null: false
    end
  end
end
