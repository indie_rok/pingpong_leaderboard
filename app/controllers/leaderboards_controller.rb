class LeaderboardsController < ApplicationController
  def show
    @leaderboard = User.order(score: :desc)
  end
end
