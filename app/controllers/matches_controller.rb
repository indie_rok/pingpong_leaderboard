class MatchesController < ApplicationController
  include MatchesHelper

  def index
    @matches = Match.where(user_a_id: current_user.id)
  end

  def new
    @user_id = current_user.id
    @matches = Match.new
  end

  def create
    new_match = Match.create(match_params)
    if new_match.persisted?
      winner = new_match.winner
      looser = new_match.looser

      rank_difference = winner.rank - looser.rank
      scoreTotal = 100 + calculateBonus(rank_difference)

      winner.increment!(:score,scoreTotal)
      winner.increment!(:games_played)
      looser.increment!(:games_played)

      reorder_ranks

      flash[:notice] = "Successfully created..."
      redirect_to matches_path
    else
      flash[:notice] = "Problems #{new_match.errors.full_messages}"

      render action: :new, :status => 422
    end
  end

  private
  def match_params
    params.require(:match).permit(:date,:user_a_id,:user_b_id,:score_a,:score_b)
  end
end
