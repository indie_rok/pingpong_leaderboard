class Match < ActiveRecord::Base

  validates_presence_of :score_a
  validates_presence_of :score_b
  validate :scores_are_greater_than_21
  validate :is_winning_by_two_points


  belongs_to :user_a, class_name: 'User', foreign_key: 'user_a_id'
  belongs_to :user_b, class_name: 'User', foreign_key: 'user_b_id'

  def scores_are_greater_than_21
    errors.add(:scores_are_greater_than_21,'One of the scores needs to be greater than 21') unless check_if_scores_are_greater_than_21
  end

  def is_winning_by_two_points
    errors.add(:two_points_advantage,"You don't have a two point advantage") unless wins_by_two_points
  end

  def check_if_scores_are_greater_than_21
    return false if self.score_a == nil or self.score_b == nil
    if self.score_a >= 21 or score_b >=21
      return true
    else
      return false
    end
  end

  def wins_by_two_points
    return false if self.score_a == nil or self.score_b == nil

    total = self.score_a - self.score_b
    if total.abs >= 2
      return true
    else
      return false
    end
  end

  def winner
    if wins_by_two_points
      if (self.score_a > self.score_b)
        user_a
      else
        user_b
      end
    else
      return "You don't have a winner"
    end
  end

  def looser
    if wins_by_two_points
      if (self.score_a > self.score_b)
        user_b
      else
        user_a
      end
    else
      return "You don't have a looser"
    end
  end

end
