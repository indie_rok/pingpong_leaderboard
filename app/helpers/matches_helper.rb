module MatchesHelper

  def calculateBonus rank_difference
    bonus = 0

    if rank_difference > 0
      bonus = User.count/rank_difference.to_f
    elsif rank_difference < 0
      bonus = rank_difference.to_f * User.count
    end

    totalBonus = bonus.abs.floor.to_i
    totalBonus
  end

  def reorder_ranks
    leaderboard = User.order(score: :desc)
    leaderboard.each_with_index do |player,index|
      player.update({rank: index +1})
    end
  end
end
