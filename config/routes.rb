Rails.application.routes.draw do
  resource :leaderboard, only:[:show]

  devise_for :users
  root to: "home#index"
  get '/history', to: 'home#history'
  get '/log',     to: 'home#log'

  resources :matches
end
