require 'rails_helper'

RSpec.describe LeaderboardsController, type: :controller do

  describe "GET #show" do
    context 'loged in user' do
      before(:each) do
       @user = Fabricate.create(:user)
       sign_in @user
     end

     it "returns http success" do
       get :show
       expect(response).to have_http_status :success
     end
    end

  end

end
