require 'rails_helper'

RSpec.describe MatchesController, type: :controller do

  describe '#new' do
    context 'loged in user' do
      before(:each) do
       @user = Fabricate.create(:user)
       sign_in @user
     end

      it 'should render the new action' do
          sign_in :user, @user
          get :new
          expect(response).to have_http_status :success
        end
      end
    end

    context 'not loged in user' do
      it 'should have an auth error' do
        get :new
        expect(response).to have_http_status (302)
      end
  end

  describe '#create' do
    context 'not loged in user' do
      it 'should not allow access' do
        post :create
        expect(response).to have_http_status (302)
      end
    end

    context 'loged in user' do
        before(:each) do
         @user = Fabricate.create(:user, email: 'indi3.rok@gmail.com')
         @user2 = Fabricate.create(:user,email: 'cool@cool.com')
         sign_in @user
       end

      context 'wrong params' do
        it 'should not allow create match when one of the scores are blanks' do
          params = {user_a_id: @user.id, user_b_id: @user2.id, score_a:nil, score_b:21,date:DateTime.now.to_date}
          post :create, match: params
          expect(flash[:notice]).to include("Score a can't be blank")
        end

        it 'should not allow create match when one of the scores are blanks' do
          params = {user_a_id: @user.id, user_b_id: @user2.id, score_a:nil, score_b:nil,date:DateTime.now.to_date}
          post :create, match: params
          expect(flash[:notice]).to include("Score a can't be blank")
        end

        it 'should not allow create match when scores are separated by less than 2' do
          params = {user_a_id: @user.id, user_b_id: @user2.id, score_a:20, score_b:21,date:DateTime.now.to_date}
          post :create, match: params
          expect(flash[:notice]).to include("You don't have a two point advantage")
        end

        it 'should not allow create match when both scores are less than 21' do
          params = {user_a_id: @user.id, user_b_id: @user2.id, score_a:20, score_b:20,date:DateTime.now.to_date}
          post :create, match: params
          expect(flash[:notice]).to include("One of the scores needs to be greater than 21")
        end
      end

      context 'good params' do

        let(:userwiner){ Fabricate.create(:user,email:'cool1@cool.com')}
        let(:userlooser){ Fabricate.create(:user,email:'asd@asd.com')}
        let(:params){ params = {user_a_id: user1.id, user_b_id: user2.id, score_a:20, score_b:20,date:DateTime.now.to_date}}


        it 'should create the event when all numbers are valid' do
          params = {user_a_id: @user.id, user_b_id: @user2.id, score_a:21, score_b:19,date:DateTime.now.to_date}
          before_count = Match.count
          post :create, match: params
          expect(Match.count).not_to eq(before_count)
        end

        it 'shoud update leaderboard' do
          params = { score_a:19,score_b:23 ,user_a_id: userwiner.id,user_b_id: userlooser.id,date: DateTime.now.to_date }
          post :create, match: params
          expect(User.find(userwiner.id).rank).to be > User.find(userlooser.id).rank
        end

      end

    end
   end

end
